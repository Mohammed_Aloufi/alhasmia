const User = require('../modules/User');
const { check, validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const md5 = require('md5')
const delay = require('delay')
let ts = Date.now();

let date_ob = new Date(ts);
let date = date_ob.getDate()
let year = date_ob.getFullYear();
let month = date_ob.getMonth() + 1;
// register functionality
const register = function (req, res) {
    const user = new User({

        name: req.body.name,
        sex: req.body.sex,
        phone: req.body.phone,
        payments: req.body.payments,
        password: req.body.password,
        role: req.body.role,
    });

    // assign errors variable to array or errors
    let errors = validationResult(req).array();

    if (errors.length > 0) {
        req.session.errors = errors;
        req.session.success = false;
       
        console.log(errors);
        res.redirect('/users/admin/addUser');
    } else {
        // hash password 
        bcrypt.genSalt(8, (err, salt) => {
            bcrypt.hash(user.password, salt, (error, hash) => {
                if (error) {
                    console.log(error);
                }
                else {
                    // if hashed correctly then register account
                    user.password = hash;
                    user.save((err, data) => {
                        if (err) {
                            
                            res.redirect('/users/admin/addUser');

                        }
                        else {
                            console.log(data);
                           
                            req.session.success = true;
                            res.redirect('/users/admin/dash');
                        }
                    });
                }
            });

        });

    }
           

    };


const login = async function(req,res,next){
   const user= await User.find({ "phone": req.body.username})
   
   
    console.log(user[0].password)
    const u =user[0].password;
    
  // await delay(5000)
  if (u == md5(req.body.password)){
      res.redirect('/users/'+user[0].role+'/dash')
  }

};


const AddPayment = async function(req,res){
    
    const user = await User.find({"phone":req.body.phone});
    console.log(user[0].payments)
    console.log(req.user.phone)
    User.updateOne(
        { phone: req.body.phone },
        { $addToSet: { payments: { month: req.body.month, amount: req.body.amount,by:req.user.name,date:year+"-"+month+"-"+date}} },
        function(err, result) {
          if (err) {
            res.send(err);
          } else {
            res.redirect('/users/admin/dash/addPayments');
          }
        }
      );
    //User.findOneAndUpdate({"phone":req.body.phone},{ payments: { month: req.body.month, amount: req.body.amount,by:req.user.name,date:year+"-"+month+"-"+date} } , {upsert: true}, function(err, doc) {
        //if (err) return res.send(500, {error: err});
    
   // });  

}

module.exports={register:register,
login:login,
AddPayment:AddPayment
}


