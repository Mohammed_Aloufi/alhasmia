const express = require('express')
const bodyParser = require('body-parser')
const path =require('path')
const app = express();
const mongoose =require('mongoose')
const passport = require('passport')
const session = require('express-session')
var port = process.env.PORT || 8080;

//set up template engine
app.set('view engine', 'ejs');

//set up middleware to parse css and js files
app.use(bodyParser.json());
app.use('*/assets', express.static(path.join(__dirname, './assets')));
app.use('*/scripts', express.static(path.join(__dirname, './script')));
app.use('*/app', express.static(path.join(__dirname, './app')));
app.use('*/', express.static(path.join(__dirname, './assets')));
app.use('/', express.static(path.join(__dirname, './assets')));
app.use('/', express.static(path.join(__dirname, './images')));
app.use('/', express.static(path.join(__dirname, './images')));
app.use('*/images', express.static(path.join(__dirname, './images')));
app.use(express.static('public'))
console.log(path.join(__dirname, './assets'))

app.use(session({
  secret: 'keyboard cat',
  resave: true,
  saveUninitialized: true,
}))

require('./config/passport')(passport);
// passport middleware session and cookies
app.use(passport.initialize());
app.use(passport.session());

app.get('/', (req, res) => {
  res.render('index')
});
const userRoute = require('./routes/routes');
app.use('/users', userRoute)
  

//set up mongoose connectinon
mongoose.set('debug', true);
mongoose.connect('mongodb+srv://mohammed:0000@todo-p93ib.mongodb.net/todo', {useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true,},
    () => {
    console.log("Connected to DB");
});

app.listen(port, () => {
  console.log('ALhashmia app listening on port 8000!')
});

