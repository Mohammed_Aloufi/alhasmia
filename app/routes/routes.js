const express = require('express');
const router = express.Router();
const User = require('../modules/User');
const userControllers = require('../controllers/userControllers');
const bodyParser = require('body-parser');
const moment = require('moment');
const delay =require('delay')
const passport = require('passport');
const { check, validationResult } = require('express-validator');
let urlencodedParser = bodyParser.urlencoded({ extended: false });

//general 
router.get('/login', (req, res) => {
    res.locals.title = "Login";
    res.render('login');
    
});
router.post('/login', urlencodedParser,  userControllers.login);
router.post('/login2', urlencodedParser,  
    passport.authenticate('local',  { failureRedirect: '/users/login/'}),
    function(req,res,next){

        if (req.user.role === "user") {
            res.locals.title = "Dashboard";
            res.render('users',{user:req.user})
        }
        else if(req.user.role === 'admin'){
            res.locals.title = "Dashboard";
            res.render('admin',{user:req.user})
            
        }
        
    
    
});

router.get('/logout', (req, res) => {
    res.locals.title = "Login";
    res.render('login');
    
});

//admin routes
router.get('/admin/dash', ensureAuthentication,(req, res) => {
    res.locals.title = "ADMIN";
    res.render('admin');
    
});

router.get('/admin/dash/addPayments',ensureAuthentication,  function(req, res) {
    // mongoose operations are asynchronous, so you need to wait 
    User.find({}, function(err, data) {
        // note that data is an array of objects, not a single object!
        console.log(data)
        res.render('enterPayments.ejs', {
            user : req.user,
            users: data
        });
    });
});
router.post('/admin/addPays',urlencodedParser ,ensureAuthentication,userControllers.AddPayment)

router.get('/admin/addUsers', (req, res) => {
    res.locals.title = "Login";
    res.render('addUsers');
    
});
router.post('/admin/addUsers', urlencodedParser,ensureAuthentication, [
    //front-end validation
    
    //check name
    check('name')
    .not()
    .isEmpty()
    .withMessage('Name is required'),
    // email must be an email
    check('phone')
    .not()
    .isEmpty()
    .withMessage('phone number is required'),
    // password must be at least 8 chars long, not common, and contain characters
    check('password')
    .not().isIn(['123', 'password', 'god'])
    .withMessage('Do not use a common word as the password')
    .isLength({ min: 8, max:8 })
    .withMessage('The password must be 8 chars long and contain a number')
    .matches(/\d/),
    
    // check if password is same
    check('password')
    .custom((val, { req, loc, path}) => {
        if( val !== req.body.confirm) {
            throw new Error("Password do not match!");
        }
        else {
            return true;
        }
    }),
    // check if role is selected
    check('role')
    .isIn(['admin', 'user'])
    .withMessage('Role is not selected'),  
    // check if sex is selected
    check('sex')
    .isIn(['male', 'female'])
    .withMessage('Sex is not selected'),      
    
    // backend validation //
    
    //check if email exist
    check('phone').custom(value => {
        return User.find({ "phone": value}).then(user => {
            if(user.length > 0) {
                return Promise.reject('Phone Number is already exist');
            }
            else {
                return true;
            }
        });
    }),
], userControllers.register)
    

router.get('/admin/viewProfile',ensureAuthentication, (req, res) => {
     // mongoose operations are asynchronous, so you need to wait 
     User.find({phone:req.user.phone}, function(err, data) {
        // note that data is an array of objects, not a single object!
        console.log(data)
        res.render('viewProfile.ejs', {
            user : req.user,
            users: data
        });
    });
    
});
router.get('/admin/editProfile', (req, res) => {
    res.locals.title = "Login";
    res.render('login');
    
});
router.get('/admin/viewPayments',ensureAuthentication, (req, res) => {
    res.render('viewPayments',{user:req.user,
    payments:req.user.payments})
    
});


//users routes
router.get('/user/viewProfile',ensureAuthentication, (req, res) => {
     // mongoose operations are asynchronous, so you need to wait 
     User.find({phone:req.user.phone}, function(err, data) {
        // note that data is an array of objects, not a single object!
        console.log(data)
        res.render('viewProfile.ejs', {
            user : req.user,
            users: data
        });
    });
    
});
router.get('/user/editProfile', (req, res) => {
    res.locals.title = "Login";
    res.render('login');
    
});
router.get('/user/viewPayments', (req, res) => {
    res.render('viewPayments',{user:req.user,
        payments:req.user.payments})
    
});
function ensureAuthentication(req, res, next){
    if(req.isAuthenticated()){
        return next();
    }
    else {
        
        res.redirect('/users/login')
    }
}

module.exports = router;
