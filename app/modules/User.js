const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
//connect to mongoDB

userSchema  = new Schema ({
    name: {
        type:String,
        required: true
    },
    phone: {
        type:String,
        required: true
    },
    sex: {
        type:String,
        enum: ['male','female'],
        required: true
    },
   
    password: {
        type:String,
        required: true
    },
    role: {
        type:String,
        enum: ['user','admin'],
        required:true
    },
    payments:[{month:String,amount:String,by:String,date:String}],
    date: {
        type:Date,
        default: Date.now
    },
   
})

userSchema.plugin(uniqueValidator);


module.exports = mongoose.model('Users', userSchema);